source $HOME/.config/nvim/plugs.vim
source $HOME/.config/nvim/keys.vim
source $HOME/.config/nvim/line.vim
set encoding=utf-8

" Used plugins, managed by vim-plug
call plug#begin('~/.vim/plugged')
  Plug 'airblade/vim-gitgutter'
  Plug 'kien/ctrlp.vim'
  Plug 'sbdchd/neoformat'
  Plug 'rbgrouleff/bclose.vim'
  Plug 'majutsushi/tagbar'
  Plug 'kshenoy/vim-signature'
  Plug 'bling/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'plytophogy/vim-virtualenv'
  Plug 'junegunn/vim-easy-align'
  Plug 'junegunn/goyo.vim'
  Plug 'junegunn/limelight.vim'
  Plug 'SirVer/ultisnips'
  Plug 'honza/vim-snippets'
  Plug 'lervag/vimtex'
  Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
  Plug 'junegunn/fzf'
  Plug 'dylanaraps/wal.vim'
  Plug 'scrooloose/syntastic'
  Plug 'Raimondi/delimitMate'
  Plug 'machakann/vim-sandwich'
  Plug 'Shougo/deoplete.nvim'
  Plug 'donRaphaco/neotex'
  Plug 'ervandew/supertab'
  Plug 'mhinz/neovim-remote'
  Plug 'tpope/vim-fugitive'
  Plug 'bling/vim-bufferline'
  Plug 'mattn/emmet-vim'
  Plug 'neovimhaskell/haskell-vim'
  Plug 'parsonsmatt/intero-neovim'
call plug#end()

" Colour scheme
syntax enable
set background=dark
colorscheme wal

" To make nvim behave well
set showcmd             " Show (partial) command in status line.
set showmatch           " Show matching brackets.
set showmode            " Show current mode.
set ruler               " Show the line and column numbers of the cursor.
set number              " Show the line numbers on the left side.
set formatoptions+=o    " Continue comment marker in new lines.
set textwidth=0         " Hard-wrap long lines as you type them.
set expandtab           " Insert spaces when TAB is pressed.
set tabstop=2           " Render TABs using this many spaces.
set shiftwidth=2        " Indentation amount for < and > commands.
set noerrorbells        " No beeps.
set modeline            " Enable modeline.
set linespace=0         " Set line-spacing to minimum.
set nojoinspaces        " Prevents inserting two spaces after punctuation on a join (J)
set lazyredraw
set rnu
set ignorecase
set smartcase
set wildignore=*/venv/*,*/.git/*,*/site/*
set laststatus=2
autocmd Filetype fortran setlocal expandtab tabstop=2 shiftwidth=4 softtabstop=4

" More natural splits
set splitbelow          " Horizontal split below current.
set splitright          " Vertical split to right of current.

" indents
