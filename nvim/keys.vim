" Sets <leader> to ,
let mapleader=","

nmap <F8> :TagbarToggle<CR>

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" Relative numbering
function! NumberToggle()
if(&relativenumber == 1)
    set nornu
    set number
else
    set rnu
endif
endfunc

" Toggle relative and absolute numbering
nnoremap <leader>r :call NumberToggle()<cr>

inoremap <expr><TAB>  pumvisible() ? "\<TAB>" : "\<C-n>"
